package bll;

import bll.validators.EmailValidator;
import bll.validators.PasswordValidator;
import bll.validators.StockValidator;
import bll.validators.Validator;
import dao.AdminDAO;
import dao.BookDAO;
import dao.ClientDAO;
import model.Admin;
import model.Book;
import model.Client;
import start.Start;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AdminBLL {
    protected static final Logger LOGGER = Logger.getLogger(Start.class.getName());
    private List<Validator<Book>> validators;

    public AdminBLL() {
        validators = new ArrayList<Validator<Book>>();
        validators.add(new StockValidator());
    }

    public Book addBook(Book book){
        for (Validator<Book> v : validators) {
            v.validate(book);
        }
        BookDAO bookDAO = new BookDAO();
        return bookDAO.insert(book);
    }

    public int deleteBook(Book book){
        BookDAO bookDAO = new BookDAO();
        return bookDAO.delete(book);
    }

    public Book updateBook(Book book){
        BookDAO bookDAO = new BookDAO();
        return bookDAO.update(book);
    }

    public Book findBookById(int id){
        BookDAO bookDAO = new BookDAO();
        return bookDAO.findById(id);
    }

    public List<Book> findAllBooks(){
        BookDAO bookDAO = new BookDAO();
        return bookDAO.findAll();
    }

    public Book findBookByName(String name){
        BookDAO bookDAO = new BookDAO();
        return bookDAO.findBookByName(name);
    }

    public Admin loginAdmin(String username, String password){
        AdminDAO adminDAO = new AdminDAO();
        Admin adminSearch = adminDAO.findAdminByName(username);
        if (adminSearch != null) {
            if (adminSearch.getPassword().equals(password))
            {
                LOGGER.log(Level.INFO, "Login Successful! ADMI User: " + username + " Pass: " + password);
                return adminSearch;
            } else{
                LOGGER.log(Level.INFO, "Login FAILED! ADMIN User: " + username + " !!! Wrong Password !!!");
                return null;
            }
        }
        LOGGER.log(Level.INFO, "Login FAILED! ADMIN User: " + username + " NOT EXISTENT");
        return null;
    }
}
