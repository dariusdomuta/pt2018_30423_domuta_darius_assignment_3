package bll;

import bll.validators.EmailValidator;
import bll.validators.PasswordValidator;
import bll.validators.Validator;
import dao.BookDAO;
import dao.ClientDAO;
import dao.TransactionDAO;
import model.Book;
import model.Client;
import model.Transaction;
import presentation.Controller;
import presentation.GUI;
import start.Start;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ClientBLL {
    protected static final Logger LOGGER = Logger.getLogger(Start.class.getName());
    private List<Validator<Client>> validators;

    public ClientBLL() {
        validators = new ArrayList<Validator<Client>>();
        validators.add(new PasswordValidator());
        validators.add(new EmailValidator());
    }
    public Client loginClinet(String username, String password){
        ClientDAO clientDAO = new ClientDAO();
        Client clientSearch = clientDAO.findClientByName(username);
        if (clientSearch != null) {
            if (clientSearch.getPassword().equals(password))
            {
                LOGGER.log(Level.INFO, "Login Successful! User: " + username + " Pass: " + password);
                return clientSearch;
            } else{
                LOGGER.log(Level.INFO, "Login FAILED! User: " + username + " !!! Wrong Password !!!");
                return null;
            }
        }
        LOGGER.log(Level.INFO, "Login FAILED! User: " + username + " NOT EXISTENT");
        return null;
    }

    public Client insertClient(Client client){
        ClientDAO clientDAO = new ClientDAO();
        for (Validator<Client> v : validators) {
            v.validate(client);
        }
        return clientDAO.insert(client);
    }

    public Book findBookById(int id){
        BookDAO bookDAO = new BookDAO();
        return bookDAO.findById(id);
    }

    public List<Book> findAllBooks(){
        BookDAO bookDAO = new BookDAO();
        return bookDAO.findAll();
    }

    public Book findBookByName(String name){
        BookDAO bookDAO = new BookDAO();
        return bookDAO.findBookByName(name);
    }

    public int buyBook (Book book, int amount){
        ClientDAO clientDAO = new ClientDAO();
        if (findBookById(book.getId()) != null) {
            if (book.getStock() >= amount){
                BookDAO bookDAO = new BookDAO();
                book.setStock(book.getStock() - amount);
                bookDAO.update(book);
                LOGGER.log(Level.WARNING, "Book with id "+ book.getId() + " successfully bought");
                DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
                LocalDateTime now = LocalDateTime.now();
                Transaction transaction = new Transaction(GUI.getCurrentClient().getId(), book.getId(), dtf.format(now), book.getPrice()*amount);
                TransactionDAO transactionDAO = new TransactionDAO();
                transactionDAO.insert(transaction);
                return 1;
            } else {
                LOGGER.log(Level.WARNING, "Book with id " + book.getId()  + "has insufficient stock, amount ordered = " + amount + " stock = " + book.getStock());
                return 0;
            }

        } else {
            LOGGER.log(Level.WARNING, "Book with id "+ book.getId() + " not found");
            return -1;
        }
    }
}
