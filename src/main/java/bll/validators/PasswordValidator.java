package bll.validators;

import model.Client;

public class PasswordValidator implements Validator<Client> {

    public void validate(Client t) {

        if (t.getPassword().length() < 6 ) {
            throw new IllegalArgumentException("Password has to have more than 6 characters!!!");
        }

    }
}
