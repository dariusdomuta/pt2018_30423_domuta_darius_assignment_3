package bll.validators;

import model.Book;

public class StockValidator implements Validator<Book> {

    public void validate(Book t) {

        if (t.getStock() < 0) {
            throw new IllegalArgumentException("Stock cannot be negative!!!");
        }

    }
}
