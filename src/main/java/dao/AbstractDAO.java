package dao;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import connection.ConnectionFactory;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Since: Apr 03, 2017
 * @Source http://www.java-blog.com/mapping-javaobjects-database-reflection-generics
 */
public class AbstractDAO<T> {
	protected static final Logger LOGGER = Logger.getLogger(AbstractDAO.class.getName());

	private final Class<T> type;

	@SuppressWarnings("unchecked")
	public AbstractDAO() {
		this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];

	}

	public String createDeleteQuery(){
		StringBuilder sb = new StringBuilder();
		sb.append("DELETE FROM ");
		sb.append(type.getSimpleName().toLowerCase());
		sb.append(" WHERE id=?");
		return sb.toString();
	}

	public String createUpdateQuery() {
		StringBuilder sb = new StringBuilder();
		sb.append("UPDATE ");
		sb.append(type.getSimpleName().toLowerCase());
		sb.append(" SET ");
		int currentIndex = 0;

		for (Field field : type.getDeclaredFields()) {
			currentIndex++;
			if(currentIndex == 1){
				continue;
			}
			field.setAccessible(true); // set modifier to public
			sb.append(field.getName());
			sb.append(" = ?,");

		}
		sb.delete(sb.length()-1, sb.length());
		sb.append(" WHERE ");
		sb.append("id = ?;");
		return sb.toString();

	}

	private String createSelectAllQuery() {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT ");
		sb.append(" * ");
		sb.append(" FROM ");
		sb.append(type.getSimpleName().toLowerCase());
		return sb.toString();
	}

	private String createSelectQuery(String field) {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT ");
		sb.append(" * ");
		sb.append(" FROM ");
		sb.append(type.getSimpleName().toLowerCase());
		sb.append(" WHERE " + field + " =?");
		return sb.toString();
	}

	public String createInsertQuery() {
		StringBuilder sb = new StringBuilder();
		sb.append("INSERT ");
		sb.append("INTO ");
		String typeName = type.getSimpleName();
		typeName = typeName.toLowerCase();
		sb.append(typeName);
		sb.append(" (");
		int currentIndex = 0;
		for (Field field : type.getDeclaredFields()) {
			currentIndex++;
			if(currentIndex == 1){
				continue;
			}
			field.setAccessible(true); // set modifier to public
			sb.append(field.getName());
			sb.append(",");
		}
		sb.delete(sb.length()-1, sb.length());
		sb.append(")");
		sb.append(" VALUES (");
		currentIndex = 0;
		for (Field field : type.getDeclaredFields()) {
			currentIndex++;
			if(currentIndex == 1){
				continue;
			}
			field.setAccessible(true); // set modifier to public
			sb.append("?,");
		}
		sb.delete(sb.length()-1, sb.length());
		sb.append(")");
		return sb.toString();
	}


	public int delete(T t){
		Connection connection = null;
		PreparedStatement statement = null;
		String query = createDeleteQuery();
		try {
			Field idField = type.getDeclaredFields()[0];
			idField.setAccessible(true);
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			statement.setInt(1, (Integer) (T) idField.get(t));
			statement.executeUpdate();
			LOGGER.log(Level.INFO, "Item was deleted");
			return 1;
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage());
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} finally {
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		LOGGER.log(Level.WARNING, "Item to DELETE not existent in the DATABASE!!!");
		return -1;
	}
	public List<T> findAll() {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createSelectAllQuery();
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			resultSet = statement.executeQuery();

			return createObjects(resultSet);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		LOGGER.log(Level.WARNING, "No items to be found in the DATABASE!!!");
		return null;
	}

	public T findById(int id) {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createSelectQuery("id");
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			statement.setInt(1, id);
			resultSet = statement.executeQuery();
			try {
				return createObjects(resultSet).get(0);
			} catch (IndexOutOfBoundsException e ){
				LOGGER.log(Level.WARNING, "No item with id " + id +" to be found in the DATABASE!!!");
			}

		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}

		return null;
	}



	public List<T> createObjects(ResultSet resultSet) {
		List<T> list = new ArrayList<T>();

		try {
			while (resultSet.next()) {
				T instance = type.newInstance();
				for (Field field : type.getDeclaredFields()) {
					Object value = resultSet.getObject(field.getName());
					PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), type);
					Method method = propertyDescriptor.getWriteMethod();
					method.invoke(instance, value);
				}
				list.add(instance);
			}
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IntrospectionException e) {
			e.printStackTrace();
		}
		return list;
	}

	public T insert(T t) {
		Connection dbConnection = ConnectionFactory.getConnection();
		String insertStatementString = createInsertQuery();
		PreparedStatement insertStatement = null;
		int insertedId = -1;
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			int currentFieldIndex = 0;
			for (Field field : type.getDeclaredFields()) {
				currentFieldIndex++;
				if (currentFieldIndex == 1) {
					continue;
				}
				field.setAccessible(true); // set modifier to public
				T value = null;
				try {
					value = (T) field.get(t);
					System.out.println(field.getName() + "=" + value);
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
				if (field.getType().equals(String.class)) {

					System.out.println("String" + (currentFieldIndex-1));
					insertStatement.setString(currentFieldIndex-1, (String) value);
				} else {
					System.out.println("Int" + (currentFieldIndex-1));
					insertStatement.setInt(currentFieldIndex-1, (Integer) value);
				}


			}
			insertStatement.executeUpdate();

			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "StudentDAO:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
		return t;
	}

	public T update(T t) {
		Connection dbConnection = ConnectionFactory.getConnection();
		String updateStatementString = createUpdateQuery();
		PreparedStatement insertStatement = null;
		int updatedId = -1;
		try {
			insertStatement = dbConnection.prepareStatement(updateStatementString, Statement.RETURN_GENERATED_KEYS);
			int currentFieldIndex = 0;
			for (Field field : type.getDeclaredFields()) {
				currentFieldIndex++;
				if (currentFieldIndex == 1) {
					continue;
				}
				field.setAccessible(true); // set modifier to public
				T value = null;
				try {
					value = (T) field.get(t);
					System.out.println(field.getName() + "=" + value);
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
				if (field.getType().equals(String.class)) {

					System.out.println("String" + (currentFieldIndex-1));
					insertStatement.setString(currentFieldIndex-1, (String) value);
				} else {
					System.out.println("Int" + (currentFieldIndex-1));
					insertStatement.setInt(currentFieldIndex-1, (Integer) value);
				}


			}
			Field idField = type.getDeclaredFields()[0];
			idField.setAccessible(true);
			insertStatement.setInt(currentFieldIndex, (Integer) (T) idField.get(t));
			insertStatement.executeUpdate();
			System.out.println(insertStatement);

		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "StudentDAO:update " + e.getMessage());
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
		return t;
	}

	public String[] getColumns() {

		String columnName[];
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSetMetaData resultSetMetaData = null;
		ResultSet resultSet = null;
		String query = createSelectAllQuery();
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			resultSet = statement.executeQuery();

			resultSetMetaData = resultSet.getMetaData();
			int nrOfColumns = resultSetMetaData.getColumnCount();
			columnName = new String[nrOfColumns + 1];
			//TODO:
			for (int i = 1; i <= nrOfColumns; i++) {
				columnName[i] = resultSetMetaData.getColumnName(i);
			}
			for (int i = 0; i < nrOfColumns; i++) {
				columnName[i] = columnName[i+1];
			}
			return columnName;
		} catch (
				SQLException e)

		{
			e.printStackTrace();
		} finally

		{
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return null;
	}

	public String[][] getData(List<T> objects) {
		
		int nrOfColumns = getColumns().length - 1;
		int nrOfObjects = objects.size();

		int line = 0;

		String[][] data = new String[nrOfObjects+1][nrOfColumns + 1];

		for (Object object : objects) {
			int column = 0;
			for (Field field : object.getClass().getDeclaredFields()) {


				field.setAccessible(true);
				Object value;

				try {

					value = field.get(object);
					data[line][column] = value.toString();
					column++;

				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
			}
			line++;
		}
		return data;
	}
}
