package dao;

import connection.ConnectionFactory;
import model.Admin;
import model.Client;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;

public class AdminDAO extends AbstractDAO<Admin> {
    private static final String FIND_ADMIN_BY_name ="SELECT * FROM admin WHERE username = ?;";
    @Override
    public int delete(Admin admin) {
        return super.delete(admin);
    }

    @Override
    public List<Admin> findAll() {
        return super.findAll();
    }

    @Override
    public Admin findById(int id) {
        return super.findById(id);
    }

    @Override
    public Admin insert(Admin admin) {
        return super.insert(admin);
    }

    @Override
    public String[] getColumns() {
        return super.getColumns();
    }

    @Override
    public String[][] getData(List<Admin> objects) {
        return super.getData(objects);
    }

    @Override
    public Admin update(Admin admin) {
        return super.update(admin);
    }

    public Admin findAdminByName(String name) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(FIND_ADMIN_BY_name);
            statement.setString(1, name);
            resultSet = statement.executeQuery();
            System.out.println(resultSet);
            try {
                return createObjects(resultSet).get(0);
            } catch (IndexOutOfBoundsException e){
                LOGGER.log(Level.WARNING, "Admin with name " + name +" not to be found in the DATABASE!!!");
            }

        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "Admin with name " + name +" not to be found in the DATABASE!!!");
            LOGGER.log(Level.WARNING,  "AdminDAO:findBookByName " + e.getMessage());
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }

        return null;
    }
}
