package dao;

import connection.ConnectionFactory;
import model.Book;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;

public class BookDAO extends AbstractDAO<Book> {

    private static final String SELECT_BOOK_BY_NAME="SELECT * FROM book WHERE name = ?";
    @Override
    public int delete(Book book) {
        return super.delete(book);
    }

    @Override
    public List<Book> findAll() {
        return super.findAll();
    }

    @Override
    public Book findById(int id) {
        return super.findById(id);
    }

    @Override
    public List<Book> createObjects(ResultSet resultSet) {
        return super.createObjects(resultSet);
    }

    @Override
    public Book insert(Book book) {
        return super.insert(book);
    }

    @Override
    public Book update(Book book) {
        return super.update(book);
    }

    @Override
    public String[] getColumns() {
        return super.getColumns();
    }

    @Override
    public String[][] getData(List<Book> objects) {
        return super.getData(objects);
    }

    public Book findBookByName(String name) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = SELECT_BOOK_BY_NAME;
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.setString(1, name);
            resultSet = statement.executeQuery();
            try {
                return createObjects(resultSet).get(0);
            } catch (IndexOutOfBoundsException e){
                LOGGER.log(Level.WARNING, "Book with name " + name +" not to be found in the DATABASE!!!");
            }

        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "Book with name " + name +" not to be found in the DATABASE!!!");
            LOGGER.log(Level.WARNING,  "BookDAO:findBookByName " + e.getMessage());
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }

        return null;
    }
}
