package dao;

import connection.ConnectionFactory;
import model.Book;
import model.Client;
import start.Start;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ClientDAO extends AbstractDAO<Client>{
    //protected static final Logger LOGGER = Logger.getLogger(Start.class.getName());
    private static final String FIND_CLIENT_BY_name ="SELECT * FROM client WHERE name = ?;";
    @Override
    public int delete(Client client) {
        return super.delete(client);
    }

    @Override
    public List<Client> findAll() {
        return super.findAll();
    }

    @Override
    public List<Client> createObjects(ResultSet resultSet) {
        return super.createObjects(resultSet);
    }

    @Override
    public Client findById(int id) {
        return super.findById(id);
    }

    @Override
    public Client insert(Client client) {
        return super.insert(client);
    }

    @Override
    public Client update(Client client) {
        return super.update(client);
    }

    @Override
    public String[] getColumns() {
        return super.getColumns();
    }

    @Override
    public String[][] getData(List<Client> objects) {
        return super.getData(objects);
    }

    public Client findClientByName(String name) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(FIND_CLIENT_BY_name);
            statement.setString(1, name);
            resultSet = statement.executeQuery();
            System.out.println(resultSet);
            try {
                return createObjects(resultSet).get(0);
            } catch (IndexOutOfBoundsException e){
                LOGGER.log(Level.WARNING, "Client with name " + name +" not to be found in the DATABASE!!!");
            }

        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "Client with name " + name +" not to be found in the DATABASE!!!");
            LOGGER.log(Level.WARNING,  "ClientDAO:findBookByName " + e.getMessage());
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }

        return null;
    }
}
