package dao;

import model.Transaction;

import java.sql.ResultSet;
import java.util.List;

public class TransactionDAO extends AbstractDAO<Transaction>{
    public TransactionDAO() {
        super();
    }

    @Override
    public int delete(Transaction transaction) {
        return super.delete(transaction);
    }

    @Override
    public List<Transaction> findAll() {
        return super.findAll();
    }

    @Override
    public Transaction findById(int id) {
        return super.findById(id);
    }

    @Override
    public List<Transaction> createObjects(ResultSet resultSet) {
        return super.createObjects(resultSet);
    }

    @Override
    public Transaction insert(Transaction transaction) {
        return super.insert(transaction);
    }

    @Override
    public Transaction update(Transaction transaction) {
        return super.update(transaction);
    }

    @Override
    public String[] getColumns() {
        return super.getColumns();
    }

    @Override
    public String[][] getData(List<Transaction> objects) {
        return super.getData(objects);
    }
}
