package model;

import java.sql.Date;

public class Transaction {
    private int id;
    private int clientId;
    private int bookId;
    private String date;
    private int price;

    public Transaction() {
    }

    public Transaction(int id, int clientId, int bookId, String date, int price) {
        this.id = id;
        this.clientId = clientId;
        this.bookId = bookId;
        this.date = date;
        this.price = price;
    }

    public Transaction(int clientId, int bookId, String date, int price) {
        this.clientId = clientId;
        this.bookId = bookId;
        this.date = date;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
