package presentation;

import model.Admin;
import model.Client;

public class Controller {
    private static Client currentClient;
    private static Admin currentAdmin;

    public static Client getCurrentClient() {
        return currentClient;
    }

    public static void setCurrentClient(Client currentClient) {
        Controller.currentClient = currentClient;
    }

    public static Admin getCurrentAdmin() {
        return currentAdmin;
    }

    public static void setCurrentAdmin(Admin currentAdmin) {
        Controller.currentAdmin = currentAdmin;
    }



}
