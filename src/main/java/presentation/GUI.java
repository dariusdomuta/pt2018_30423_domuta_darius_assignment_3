package presentation;

import bll.AdminBLL;
import bll.ClientBLL;
import dao.BookDAO;
import dao.ClientDAO;
import model.Admin;
import model.Book;
import model.Client;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GUI extends JFrame{
    private static Client currentClient;
    private static Admin currentAdmin;

    private static final long serialVersionUID = 1L;
    private JPanel panel;
    private int WIDTH = 600, HEIGHT = 600;

    public GUI() {
        panel = new JPanel();
        this.add(panel);
        panel.setLayout(null);
        this.setSize(WIDTH, HEIGHT);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setVisible(true);

    }

    public static Client getCurrentClient() {
        return currentClient;
    }

    public static void setCurrentClient(Client currentClient) {
        GUI.currentClient = currentClient;
    }

    public static Admin getCurrentAdmin() {
        return currentAdmin;
    }

    public static void setCurrentAdmin(Admin currentAdmin) {
        GUI.currentAdmin = currentAdmin;
    }

    public void chooseRights(){
        panel.removeAll();
        panel.revalidate();
        panel.setLayout(null);

        JButton clientBt = new JButton("Client");
        clientBt.setBounds(40,40, 100, 40);
        panel.add(clientBt);

        JButton adminBt = new JButton("Admin");
        adminBt.setBounds(40,120, 100, 40);
        panel.add(adminBt);

        clientBt.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                clientLogin();
            }
        });

        adminBt.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                adminLogin();
            }
        });
    }

    public void clientLogin(){
        panel.removeAll();
        panel.setLayout(null);

        JLabel usernameLabel= new JLabel("username: ");
        usernameLabel.setBounds(40,40, 100, 30);

        JTextField usernameField = new JTextField();
        usernameField.setBounds(140, 40, 100, 30);

        JLabel passLabel= new JLabel("password: ");
        passLabel.setBounds(40,80, 100, 30);

        JTextField passField = new JTextField();
        passField.setBounds(140, 80, 100, 30);

        JButton loginBt = new JButton("LOGIN");
        loginBt.setBounds(40,180,100,30);

        JLabel errorLabel= new JLabel();
        errorLabel.setBounds(40,300, 300, 30);

        loginBt.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ClientBLL clientBLL = new ClientBLL();
                currentClient = null;
                currentClient = clientBLL.loginClinet(usernameField.getText(), passField.getText());
                if (currentClient == null) {
                    errorLabel.setText("Wrong username or password!!!");
                }
                else {
                    //TODO:
                    clientInterface();
                }
            }
        });

        panel.add(usernameField);
        panel.add(usernameLabel);
        panel.add(passField);
        panel.add(passLabel);
        panel.add(loginBt);
        panel.add(errorLabel);

        panel.repaint();
        panel.revalidate();
    }

    public void adminLogin(){
        panel.removeAll();
        panel.setLayout(null);

        JLabel usernameLabel= new JLabel("username: ");
        usernameLabel.setBounds(40,40, 100, 30);

        JTextField usernameField = new JTextField();
        usernameField.setBounds(140, 40, 100, 30);

        JLabel passLabel= new JLabel("password: ");
        passLabel.setBounds(40,80, 100, 30);

        JTextField passField = new JTextField();
        passField.setBounds(140, 80, 100, 30);

        JButton loginBt = new JButton("LOGIN");
        loginBt.setBounds(40,180,100,30);

        JLabel errorLabel= new JLabel();
        errorLabel.setBounds(40,300, 300, 30);

        loginBt.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                AdminBLL adminBLL = new AdminBLL();
                currentAdmin = null;
                currentAdmin = adminBLL.loginAdmin(usernameField.getText(), passField.getText());
                if (currentAdmin == null) {
                    errorLabel.setText("Wrong username or password!!!");
                }
                else {
                    adminInterface();
                }
            }
        });

        panel.add(usernameField);
        panel.add(usernameLabel);
        panel.add(passField);
        panel.add(passLabel);
        panel.add(loginBt);
        panel.add(errorLabel);

        panel.repaint();
        panel.revalidate();
    }

    public void clientInterface() {
        panel.removeAll();
        panel.setLayout(null);

        BookDAO bookDAO = new BookDAO();

        JLabel buyLabel = new JLabel("Choose the name of the book you want to buy:");
        buyLabel.setBounds(50,20,300,30);

        JTextField bookNameTF = new JTextField();
        bookNameTF.setBounds(50,50,100, 30);

        JLabel stockLabel = new JLabel("Choose how many books you want to buy:");
        stockLabel.setBounds(50,80,300,30);

        JTextField stockTF = new JTextField();
        stockTF.setBounds(50,110,100, 30);

        JButton buyButton = new JButton("BUY");
        buyButton.setBounds(50, 140, 100, 30);

        int buyResult = 0;
        buyButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ClientBLL clientBLL = new ClientBLL();

                JLabel messageLabel = new JLabel("result");

                messageLabel.setBounds(50, 210, 300, 30);


                int buyResult = 0;
                try{
                    buyResult = clientBLL.buyBook(bookDAO.findBookByName(bookNameTF.getText()), Integer.parseInt(stockTF.getText()));
                } catch (NullPointerException ex) {
                    panel.add(messageLabel);
                    ex.printStackTrace();
                } finally {
                    if (buyResult == 1) messageLabel.setText("Item successfully purchased!");
                    if (buyResult == 0) messageLabel.setText("Not in stock!");
                    if (buyResult == -1) messageLabel.setText("Book not found!");

                }

            }
        });



        JButton showBooksBt = new JButton("SHOW BOOKS");
        showBooksBt.setBounds(50,170, 200, 30);



        showBooksBt.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JTable jTable = new JTable(bookDAO.getData(bookDAO.findAll()),bookDAO.getColumns());
                JScrollPane scrollPane = new JScrollPane(jTable);
                scrollPane.setBounds(50,250,400,200);
                panel.add(scrollPane);

                JLabel messageLabel = new JLabel("lol");
                messageLabel.setBounds(50, 190, 300, 30);
                panel.add(messageLabel);
            }
        });

        JTable jTable = new JTable(bookDAO.getData(bookDAO.findAll()),bookDAO.getColumns());
        JScrollPane scrollPane = new JScrollPane(jTable);
        scrollPane.setBounds(50,250,400,200);
        panel.add(scrollPane);




        panel.add(buyLabel);
        panel.add(bookNameTF);
        panel.add(stockLabel);
        panel.add(stockTF);
        panel.add(buyButton);
        panel.add(showBooksBt);

        panel.repaint();
        panel.revalidate();
    }

    public void adminInterface(){
        panel.removeAll();
        panel.setLayout(null);

        JButton insertButton = new JButton("INSERT BOOK");
        insertButton.setBounds(40, 40, 200, 30);
        panel.add(insertButton);

        JButton updateButton = new JButton("UPDATE BOOK");
        updateButton.setBounds(40, 80, 200, 30);
        panel.add(updateButton);

        JButton deleteButton = new JButton("DELETE BOOK");
        deleteButton.setBounds(40, 123, 200, 30);
        panel.add(deleteButton);

        JButton findALLButton = new JButton("FIND ALL BOOKS");
        findALLButton.setBounds(40, 160, 200, 30);
        panel.add(findALLButton);

        JButton insertClientButton = new JButton("INSERT Client");
        insertClientButton.setBounds(300, 40, 200, 30);
        panel.add(insertClientButton);

        JButton updateClientButton = new JButton("UPDATE Client");
        updateClientButton.setBounds(300, 80, 200, 30);
        panel.add(updateClientButton);

        JButton deleteClientButton = new JButton("DELETE Client");
        deleteClientButton.setBounds(300, 123, 200, 30);
        panel.add(deleteClientButton);

        JButton findALLClientsButton = new JButton("FIND ALL Clients");
        findALLClientsButton.setBounds(300, 160, 200, 30);
        panel.add(findALLClientsButton);

        insertButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                insertBook();
            }
        });

        updateButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                updateBook();
            }
        });

        deleteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                deleteBook();
            }
        });

        findALLButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                findAllBooks();
            }
        });
        
        insertClientButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                insertClient();
            }
        });

        updateClientButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                updateClient();
            }
        });

        deleteClientButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                deleteClient();
            }
        });

        findALLClientsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                findAllClients();
            }
        });


        panel.repaint();
        panel.revalidate();

    }

    public void insertBook(){
        panel.removeAll();
        panel.setLayout(null);

        JLabel nameLabel = new JLabel("name: ");
        nameLabel.setBounds(40, 40, 100, 30);
        panel.add(nameLabel);

        JTextField nameTf = new JTextField();
        nameTf.setBounds(150, 40, 100, 30);
        panel.add(nameTf);

        JLabel authorLabel = new JLabel("author: ");
        authorLabel.setBounds(40, 80, 100, 30);
        panel.add(authorLabel);

        JTextField authorTf = new JTextField();
        authorTf.setBounds(150, 80, 100, 30);
        panel.add(authorTf);

        JLabel priceLabel = new JLabel("price: ");
        priceLabel.setBounds(40, 120, 100, 30);
        panel.add(priceLabel);

        JTextField priceTf = new JTextField();
        priceTf.setBounds(150, 120, 100, 30);
        panel.add(priceTf);

        JLabel stockLabel = new JLabel("stock: ");
        stockLabel.setBounds(40, 160, 100, 30);
        panel.add(stockLabel);

        JTextField stockTf = new JTextField();
        stockTf.setBounds(150, 160, 100, 30);
        panel.add(stockTf);

        JButton insertButton = new JButton("INSERT");
        insertButton.setBounds(40, 200, 100, 30);
        panel.add(insertButton);

        insertButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Book book = new Book(nameTf.getText(), authorTf.getText(), Integer.parseInt(priceTf.getText()), Integer.parseInt(stockTf.getText()));
                BookDAO bookDAO = new BookDAO();
                bookDAO.insert(book);
                adminInterface();
            }
        });

        panel.repaint();
        panel.revalidate();

    }

    public void updateBook(){
        panel.removeAll();
        panel.setLayout(null);

        JLabel idLabel = new JLabel("id: ");
        idLabel.setBounds(40, 10, 100, 30);
        panel.add(idLabel);

        JTextField idTf = new JTextField();
        idTf.setBounds(150, 10, 100, 30);
        panel.add(idTf);

        JLabel nameLabel = new JLabel("name: ");
        nameLabel.setBounds(40, 40, 100, 30);
        panel.add(nameLabel);

        JTextField nameTf = new JTextField();
        nameTf.setBounds(150, 40, 100, 30);
        panel.add(nameTf);

        JLabel authorLabel = new JLabel("author: ");
        authorLabel.setBounds(40, 80, 100, 30);
        panel.add(authorLabel);

        JTextField authorTf = new JTextField();
        authorTf.setBounds(150, 80, 100, 30);
        panel.add(authorTf);

        JLabel priceLabel = new JLabel("price: ");
        priceLabel.setBounds(40, 120, 100, 30);
        panel.add(priceLabel);

        JTextField priceTf = new JTextField();
        priceTf.setBounds(150, 120, 100, 30);
        panel.add(priceTf);

        JLabel stockLabel = new JLabel("stock: ");
        stockLabel.setBounds(40, 160, 100, 30);
        panel.add(stockLabel);

        JTextField stockTf = new JTextField();
        stockTf.setBounds(150, 160, 100, 30);
        panel.add(stockTf);

        JButton updateButton = new JButton("update");
        updateButton.setBounds(40, 200, 100, 30);
        panel.add(updateButton);

        updateButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Book book = new Book(Integer.parseInt(idTf.getText()),nameTf.getText(), authorTf.getText(), Integer.parseInt(priceTf.getText()), Integer.parseInt(stockTf.getText()));
                BookDAO bookDAO = new BookDAO();
                bookDAO.update(book);
                adminInterface();
            }
        });

        panel.repaint();
        panel.revalidate();
    }
    public void deleteBook() {
        panel.removeAll();
        panel.setLayout(null);

        JLabel idLabel = new JLabel("id: of the book to delete ");
        idLabel.setBounds(40, 40, 200, 30);
        panel.add(idLabel);

        JTextField idTf = new JTextField();
        idTf.setBounds(250, 40, 100, 30);
        panel.add(idTf);

        JButton deleteButton = new JButton("DELETE");
        deleteButton.setBounds(40,80, 100, 30);
        panel.add(deleteButton);

        deleteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                BookDAO bookDAO = new BookDAO();
                bookDAO.delete(bookDAO.findById(Integer.parseInt(idTf.getText())));
                adminInterface();
            }
        });

        panel.repaint();
        panel.revalidate();

    }

    public void findAllBooks() {
        BookDAO bookDAO = new BookDAO();
        JTable jTable = new JTable(bookDAO.getData(bookDAO.findAll()),bookDAO.getColumns());
        JScrollPane scrollPane = new JScrollPane(jTable);
        scrollPane.setBounds(50,250,400,200);
        panel.add(scrollPane);
    }

    public void insertClient(){
        panel.removeAll();
        panel.setLayout(null);

        JLabel nameLabel = new JLabel("name: ");
        nameLabel.setBounds(40, 40, 100, 30);
        panel.add(nameLabel);

        JTextField nameTf = new JTextField();
        nameTf.setBounds(150, 40, 100, 30);
        panel.add(nameTf);

        JLabel PasswordLabel = new JLabel("Password: ");
        PasswordLabel.setBounds(40, 80, 100, 30);
        panel.add(PasswordLabel);

        JTextField PasswordTf = new JTextField();
        PasswordTf.setBounds(150, 80, 100, 30);
        panel.add(PasswordTf);

        JLabel EmailLabel = new JLabel("Email: ");
        EmailLabel.setBounds(40, 120, 100, 30);
        panel.add(EmailLabel);

        JTextField EmailTf = new JTextField();
        EmailTf.setBounds(150, 120, 100, 30);
        panel.add(EmailTf);

        JButton insertButton = new JButton("INSERT");
        insertButton.setBounds(40, 200, 100, 30);
        panel.add(insertButton);

        insertButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Client Client = new Client(nameTf.getText(), PasswordTf.getText(), EmailTf.getText());
                ClientDAO ClientDAO = new ClientDAO();
                ClientDAO.insert(Client);
                adminInterface();
            }
        });

        panel.repaint();
        panel.revalidate();

    }

    public void updateClient(){
        panel.removeAll();
        panel.setLayout(null);

        JLabel idLabel = new JLabel("id: ");
        idLabel.setBounds(40, 10, 100, 30);
        panel.add(idLabel);

        JTextField idTf = new JTextField();
        idTf.setBounds(150, 10, 100, 30);
        panel.add(idTf);

        JLabel nameLabel = new JLabel("name: ");
        nameLabel.setBounds(40, 40, 100, 30);
        panel.add(nameLabel);

        JTextField nameTf = new JTextField();
        nameTf.setBounds(150, 40, 100, 30);
        panel.add(nameTf);

        JLabel PasswordLabel = new JLabel("Password: ");
        PasswordLabel.setBounds(40, 80, 100, 30);
        panel.add(PasswordLabel);

        JTextField PasswordTf = new JTextField();
        PasswordTf.setBounds(150, 80, 100, 30);
        panel.add(PasswordTf);

        JLabel EmailLabel = new JLabel("Email: ");
        EmailLabel.setBounds(40, 120, 100, 30);
        panel.add(EmailLabel);

        JTextField EmailTf = new JTextField();
        EmailTf.setBounds(150, 120, 100, 30);
        panel.add(EmailTf);

        JButton updateButton = new JButton("update");
        updateButton.setBounds(40, 200, 100, 30);
        panel.add(updateButton);

        updateButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Client Client = new Client(Integer.parseInt(idTf.getText()),nameTf.getText(), PasswordTf.getText(), EmailTf.getText());
                ClientDAO ClientDAO = new ClientDAO();
                ClientDAO.update(Client);
                adminInterface();
            }
        });

        panel.repaint();
        panel.revalidate();

    }

    public void deleteClient() {
        panel.removeAll();
        panel.setLayout(null);

        JLabel idLabel = new JLabel("id: of the Client to delete ");
        idLabel.setBounds(40, 40, 200, 30);
        panel.add(idLabel);

        JTextField idTf = new JTextField();
        idTf.setBounds(250, 40, 100, 30);
        panel.add(idTf);

        JButton deleteButton = new JButton("DELETE");
        deleteButton.setBounds(40,80, 100, 30);
        panel.add(deleteButton);

        deleteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ClientDAO ClientDAO = new ClientDAO();
                ClientDAO.delete(ClientDAO.findById(Integer.parseInt(idTf.getText())));
                adminInterface();
            }
        });

        panel.repaint();
        panel.revalidate();

    }

    public void findAllClients() {
        ClientDAO ClientDAO = new ClientDAO();
        JTable jTable = new JTable(ClientDAO.getData(ClientDAO.findAll()),ClientDAO.getColumns());
        JScrollPane scrollPane = new JScrollPane(jTable);
        scrollPane.setBounds(50,250,400,200);
        panel.add(scrollPane);
    }



}
