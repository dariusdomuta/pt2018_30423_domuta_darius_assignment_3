package start;

import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import bll.AdminBLL;
import dao.AdminDAO;
import dao.BookDAO;
import dao.ClientDAO;
import model.Admin;
import model.Book;
import model.Client;
import presentation.GUI;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Since: Apr 03, 2017
 */
public class Start {
	protected static final Logger LOGGER = Logger.getLogger(Start.class.getName());

	public static void main(String[] args) throws SQLException {

		BookDAO bookDAO = new BookDAO();
		System.out.println(bookDAO.getColumns()[1].toString());
		GUI gui = new GUI();
		gui.chooseRights();
		Book book = new Book("Amintiri", "Creanga", 200, 150);
		bookDAO.insert(book);

	}
}
		//Admin admin = new Admin("Domu", "admin");
		//Client client = new Client("Domuta", "client", "dariusclient.com");
		//Book book = new Book("Lala", "Band", 150, 22);

		//StudentDAO studentDAO = new StudentDAO();
		//System.out.println(studentDAO.findStudentByName("gusti"));
		//BookDAO bookDAO = new BookDAO();
		//System.out.println(bookDAO.findBookByName("Poezii").toString());

		//ClientDAO clientDAO = new ClientDAO();
		//System.out.println(clientDAO.getData(clientDAO.findAll())[1][1].toString());
		//System.out.println(clientDAO.findById(5).getName().toString());
		//System.out.println(clientDAO.findClientByName("client").getId());

		//clientDAO.insert(client);

		/*ClientBLL clientBLL = new ClientBLL();
		Client currentClient = clientBLL.loginClinet("Domuta", "client");
		System.out.println(currentClient.getName());
		if (currentClient != null) {
			Controller.setCurrentClient(currentClient);
			clientBLL.buyBook(clientBLL.findBookByName("Ortu"), 1);
		}*/


		//AdminBLL adminBLL = new AdminBLL();
		//AdminDAO adminDAO = new AdminDAO();
		//adminDAO.insert(admin);
		//Admin currentAdmin = adminBLL.loginAdmin("Domu", "adminn");




		//System.out.println(clientBLL.buyBook(bookDAO.findBookByName("Ortu"), 2));


		// Generate error
		//try {
			//System.out.println(studentBll.findAllStudents().get(1));
		//} catch (Exception ex) {
			//LOGGER.log(Level.INFO, ex.getMessage() + "cacat");


		//obtain field-value pairs for object through reflection
		//ReflectionExample.retrieveProperties(student);


		//obtain field-value pairs for object through reflection
		//ReflectionExample.retrieveProperties(student);




